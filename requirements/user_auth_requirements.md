# Game App Integration Testing

## User Routes Integration Testing

### Login User 
**success-**
- Respon status 200
- Nama dan password terisi
- Respon mengandung token

**invalid name or password-**
- Respon status 401
- Name dan password terisi
- Respon unauthorized

**required field violation-**
- Respon status 400
- Name atau password kosong
- Respon bad request

### Tambah User Baru
**success-**
- Respon status 201
- Nama dan password terisi
- Respon mengandung properti message dengan pesan "Berhasil menambahkan user"

**no auth-**
- Respon status 401 
- Tidak terdapat header authorization
- name dan password diisi

**invalid token-**
- Respon status 401 
- Terdapat header authorization
- name dan password diisi
- Header authorization invalid

**required field violation-**
- Respon status 400
- Name atau password kosong
- Respon bad request

**password length less than 5-**
- Respon status 400
- Name atau password terisi
- Panjang password kurang dari 5 karakter
- Respon bad request


### Lihat Daftar User
**success-**
- Respon status 200
- Respon berupa array
- Respon mengandung properti id dan name 

**success found users with similar name by query params-**
- Respon status 200
- Respon berupa array
- Respon mengandung properti id dan name

**no auth-**
- Respon status 401 
- Tidak terdapat header authorization


**invalid token-**
- Respon status 401 
- Terdapat header authorization
- Header authorization invalid

**Users not found with query params-**
- Respon status 404
- Respon not found


### Lihat User Berdasar Id
**success-**
- Respon status 200
- Respon berupa objek
- Respon mengandung properti id dan name saja

**no auth-**
- Respon status 401 
- Tidak terdapat header authorization

**invalid token-**
- Respon status 401 
- Terdapat header authorization
- Header authorization invalid

**User not found-**
- Respon status 404
- Respon not found

### Update User
**success-**
- Respon status 200
- Nama dan password terisi
- Respon mengandung properti message dengan pesan "Berhasil update data"

**success with only one field-**
- Respon status 200
- Nama atau password kosong
- Respon mengandung properti message dengan pesan "Berhasil update data"

**required field violation-**
- Respon status 400
- Name dan password kosong (dua-duanya)
- Respon bad request

**no auth-**
- Respon status 401 
- Tidak terdapat header authorization
- name dan password diisi

**invalid token-**
- Respon status 401 
- Terdapat header authorization
- name dan password diisi
- Header authorization invalid

**user who is going to be updated not found-**
- Respon status 404
- Respon not found

### Delete User
**success-**
- Respon status 200
- Respon mengandung properti message dengan pesan "Berhasil menghapus data"

**invalid userId parameter-**
- Respon status 400
- Respon bad request

**no auth-**
- Respon status 401 
- Tidak terdapat header authorization

**invalid token-**
- Respon status 401 
- Terdapat header authorization
- Header authorization invalid

**user not found-**
- Respon status 404
- Respon not found




