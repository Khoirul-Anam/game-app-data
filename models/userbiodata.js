'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserBiodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      UserBiodata.belongsTo(models.UserGame, {foreignKey: 'username_id', as: 'UserGame'});
      UserBiodata.hasMany(models.UserGameHistory, {foreignKey: 'user_bio_id', as: 'histories'})
    }
  }
  UserBiodata.init({
    player_name: DataTypes.STRING,
    username_id: DataTypes.INTEGER,
    language: DataTypes.ENUM('English', 'French', 'Russian', 'Japanese', 'Arab', 'Chinese'),
    money: DataTypes.BIGINT,
    diamond: DataTypes.INTEGER,
    rank: DataTypes.ENUM('newbie', 'rookie', 'senior', 'pro', 'legend')
  }, {
    sequelize,
    modelName: 'UserBiodata',
  });
  
  return UserBiodata;
};