'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserGameHistory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      UserGameHistory.belongsTo(models.UserBiodata, {foreignKey: 'user_bio_id', as: 'UserBiodata'})
    }
  }
  UserGameHistory.init({
    match_type: DataTypes.ENUM('casual', 'rank', 'ai', 'brawl'),
    match_date: DataTypes.DATE,
    duration_inmin: DataTypes.INTEGER,
    result: DataTypes.ENUM('lose', 'win'),
    user_bio_id: DataTypes.INTEGER,
    money_earned: DataTypes.BIGINT,
    rank_change: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'UserGameHistory',
  });
  return UserGameHistory;
};