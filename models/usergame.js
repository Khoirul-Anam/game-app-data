"use strict";
const { Model } = require("sequelize");
const bcrypt = require('bcryptjs');
module.exports = (sequelize, DataTypes) => {
  class UserGame extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      
    }
  }
  UserGame.init(
    {
      name: DataTypes.STRING,
      password: DataTypes.STRING,
      video_profile: DataTypes.STRING,
      email: DataTypes.STRING,
      forgot_pass_token: DataTypes.STRING,
      forgot_pass_token_expired_at: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "UserGame",
    }
  );

  UserGame.addHook('beforeCreate', (model, options)=>{
    const salt = bcrypt.genSaltSync(10);
    model.password = bcrypt.hashSync(model.password, salt);
  });

  UserGame.addHook('beforeUpdate', (model, options)=>{
    const salt = bcrypt.genSaltSync(10);
    model.password = bcrypt.hashSync(model.password, salt);
  })
  return UserGame;
};
