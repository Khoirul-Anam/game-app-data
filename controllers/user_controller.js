const ErrorResponse = require("../error_messages");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const { UserGame } = require("../models");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const LoginMethod = require("../services/login_service");
const MessageTemplate = require("../services/msg_template_service");
const sendEmail = require("../services/email_service");
const otpGenerator = require("otp-generator");

class UserGameController {
  static async addNewUser(req, res, next) {
    try {
      const { name, email, password } = req.body;
      console.log(req.body);
      if (!name || !password || !email || !req.file) throw ErrorResponse.badReq;
      if (password.length < 5) throw ErrorResponse.badReq;
      const user = await UserGame.create({
        name,
        password,
        email,
        video_profile: req.file.path,
        createdAt: new Date(),
        updatedAt: new Date(),
      });
      const messageContent = MessageTemplate.getRegMsg(user.name);
      res.status(201).json({
        status: 201,
        message: "Berhasil menambahkan user",
      });
      await sendEmail(
        process.env.SERVER_EMAIL,
        user.email,
        null,
        messageContent.message,
        messageContent.subject
      );
    } catch (error) {
      console.log(error);
      next(error);
    }
  }

  static async getAllUser(req, res, next) {
    try {
      let users;
      const { name } = req.query;
      if (name !== undefined) {
        users = await UserGame.findAll({
          attributes: ["id", "name", "email"], //merahasiakan password
          where: {
            name: {
              [Op.or]: [
                { [Op.like]: `%${name}%` },
                { [Op.like]: `${name}%` },
                { [Op.like]: `%${name}` },
              ],
            },
          },
        });
      } else {
        users = await UserGame.findAll({
          attributes: ["id", "name", "email"], //merahasiakan password
        });
      }
      if (users.length > 0) {
        res.status(200).json(users);
        return;
      }
      throw ErrorResponse.notFound;
    } catch (error) {
      console.log(error);
      next(error);
    }
  }

  static async login(req, res, next) {
    try {
      let token;
      const { name, password, google_id_token, facebook_id_token } = req.body;
      if (google_id_token) {
        token = await LoginMethod.googleLogin(google_id_token);
        return res.status(200).json({
          your_token: token,
        });
      }
      if (facebook_id_token) {
        token = await LoginMethod.facebookLogin(facebook_id_token);
        return res.status(200).json({
          your_token: token,
        });
      }
      if (name && password) {
        token = await LoginMethod.manualLogin(name, password);
        return res.status(200).json({
          your_token: token,
        });
      }
    } catch (error) {
      next(error);
    }
  }

  static async getUserById(req, res, next) {
    try {
      const { id } = req.params;
      const user = await UserGame.findOne({
        attributes: ["id", "name"],
        where: {
          id: id,
        },
      });
      if (user === null) {
        throw ErrorResponse.notFound;
      }
      return res.status(200).json({
        status: 200,
        id: user.id,
        name: user.name,
      });
    } catch (error) {
      console.log(error);
      next(error);
    }
  }

  static async updateUser(req, res, next) {
    try {
      const { id } = req.params;
      const { name, password } = req.body;
      const user = await UserGame.findOne({
        where: {
          id: id,
        },
      });
      console.log(id);
      console.log(user);
      if (user === null) throw ErrorResponse.notFound;

      const badFormat = name === undefined && password === undefined;
      if (badFormat) throw ErrorResponse.badReq;
      await UserGame.update(
        {
          name: name === undefined ? user.name : name,
          password: password === undefined ? user.password : password,
          updatedAt: new Date(),
        },
        {
          where: {
            id: id,
          },
          individualHooks: true,
          returning: true,
        }
      );
      res.status(200).json({
        status: 200,
        message: "Berhasil update data",
      });
    } catch (error) {
      console.log(error);
      next(error);
    }
  }

  static async deleteUser(req, res, next) {
    try {
      const { id } = req.params;

      const user = await UserGame.findOne({
        where: {
          id: id,
        },
      });
      if (user === null) throw ErrorResponse.notFound;
      await UserGame.destroy({
        where: {
          id: id,
        },
      });
      res.status(200).json({
        status: 200,
        message: "Berhasil menghapus data",
      });
    } catch (error) {
      console.log(error);
      next(error);
    }
  }

  static async sendForgotPassToken(req, res, next) {
    try {
      const { email } = req.body;
      if (!email) throw ErrorResponse.invalidEmail;
      const user = await UserGame.findOne({
        where: {
          email,
        },
      });
      if (!user) throw ErrorResponse.invalidEmail;
      const otp = otpGenerator.generate(6, {
        upperCaseAlphabets: false,
        specialChars: false,
        digits: true,
        lowerCaseAlphabets: false
      });

      const salt = bcrypt.genSaltSync(10);
      const hashedOtp = bcrypt.hashSync(otp, salt);
      await UserGame.update(
        {
          forgot_pass_token: hashedOtp,
          forgot_pass_token_expired_at: new Date(
            new Date().getTime() + 5 * 60000
          ),
        },
        {
          where: {
            email,
          },
        }
      );

      const messageContent = MessageTemplate.getFrgPassMsg(
        otp,
        new Date()
      );
      await sendEmail(
        process.env.SERVER_EMAIL,
        email,
        null,
        messageContent.message,
        messageContent.subject
      );
      res.status(200).json({
        message: "Silahkan cek email anda untuk token otp",
      });
    } catch (error) {
      next(error);
    }
  }

  static async verifyPassToken(req, res, next) {
    try {
      const { email, token } = req.body;
      if (!email || !token ) throw ErrorResponse.sendCustomResponse(400, "Invalid user or token");
      const user = await UserGame.findOne({
        where: {
          email,
        },
      });
      if (!user) throw ErrorResponse.invalidEmail;
      if (!bcrypt.compareSync(token, user.forgot_pass_token))
        throw ErrorResponse.sendCustomResponse(400, "Invalid Token");
      if (user.forgot_pass_token < new Date())
        throw ErrorResponse.sendCustomResponse(400, "Invalid Token");

      res.status(200).json({
        valid: true,
        message: "Token is valid",
      });
    } catch (error) {
      next(error);
    }
  }

  static async changePassword(req, res, next) {
    try {
      const { email, token, password, password_confirmation } = req.body;
      if (!email || !token || !password || !password_confirmation ) throw ErrorResponse.badReq
      if (password !== password_confirmation)
        throw ErrorResponse.sendCustomResponse(
          400,
          "Password does not match password confirmation"
        );
      const user = await UserGame.findOne({
        where: {
          email,
        },
      });

      if (!user)
        throw ErrorResponse.sendCustomResponse(400, "Invalid user or token");
      if (!bcrypt.compareSync(token, user.forgot_pass_token))
        throw ErrorResponse.sendCustomResponse(400, "Invalid user or token");
      if (user.forgot_pass_token_expired_at < new Date())
        throw ErrorResponse.sendCustomResponse(400, "Invalid user or token");

      const salt = bcrypt.genSaltSync(10);
      const hashedPassword = bcrypt.hashSync(password, salt);
      await UserGame.update(
        {
          password: hashedPassword,
          forgot_pass_token: null,
          forgot_pass_token_expired_at: null,
          updatedAt: new Date(),
        },
        {
          where: {
            email,
          },
        }
      );
      res.status(200).json({
        message: "Successfully change password",
      });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = UserGameController;
