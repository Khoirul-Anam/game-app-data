const ErrorResponse = require("../error_messages");
const { UserGameHistory } = require("../models");


class GameHistoryController {
  static async getAllHistories(req, res, next) {
    try {
      const allHistories = await UserGameHistory.findAll({
        attributes: ['id', 'match_type', 'match_date']
      });
      if (allHistories === null) throw ErrorResponse.notFound;
      res.status(200).json(
        allHistories.map((history) => ({
          id:history.id,
          match_type:history.match_type,
          match_date : history.match_date,
        }))
      );
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
  static async getHistoryById(req, res, next) {
    const { id } = req.params;
    try {
      const history = await UserGameHistory.findOne({
        where: {
          id: id,
        },
      });
      if (history === null) throw ErrorResponse.notFound;
      res.status(200).json({
        id:history.id,
        match_type:history.match_type,
        match_date : history.match_date,
        duration_inmin: history.duration_inmin,
        result: history.result,
        user_bio_id: history.user_bio_id,
        money_earned: Number(history.money_earned),
        createdAt: history.createdAt,
        updatedAt: history.updatedAt,
      });
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
  
}

module.exports = GameHistoryController;
