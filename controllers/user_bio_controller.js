const ErrorResponse = require("../error_messages");
const { UserBiodata, UserGameHistory } = require("../models");

// const jwt = require('jsonwebtoken');

class UserBiodataController {
  static async getAllBio(_req, res, next) {
    try {
      console.log(UserGameHistory);
      const allBio = await UserBiodata.findAll({
        attributes: ["id", "player_name"],
      });
      res.status(200).json(
        allBio.map((bio) => ({
          id: bio.id,
          player_name: bio.player_name,
        }))
      );
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
  static async getPlayerBio(req, res, next) {
    const { id } = req.params;
    try {
      const bio = await UserBiodata.findOne({
        where: {
          id: id,
        },
      });
      console.log(bio);
      if (bio === null) throw ErrorResponse.notFound;
      if (bio.username_id !== req.user.id) throw ErrorResponse.unAuth;
      res.status(200).json({
        id: bio.id,
        player_name: bio.player_name,
        username_id: bio.username_id,
        language: bio.language,
        money: Number(bio.money),
        diamond: bio.diamond,
        rank: bio.rank,
        createdAt: bio.createdAt,
        updatedAt: bio.updatedAt,
      });
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
  static async getPlayerMatches(req, res, next) {
    try {
      const { name } = req.user;
      const { id } = req.params;
      const matches = await UserBiodata.findOne({
        include: {
          model: UserGameHistory,
          as: "histories",
          attributes: [
            "id",
            "user_bio_id",
            "match_type",
            "match_date",
            "result",
          ],
        },
        where: {
          id,
        },
        // attributes: ['player_name', 'id', 'match_type', 'match_date', 'result']
      });
      if (matches === null) throw ErrorResponse.notFound;
      if (matches.username_id !== req.user.id)
        throw ErrorResponse.unAuth;
      res.status(200).json({
        user: name,
        match_detail: matches,
      });
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
}

module.exports = UserBiodataController;
