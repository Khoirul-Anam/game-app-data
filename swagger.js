const swaggerAutogen = require("swagger-autogen")();

const outputFile = "./swagger_output.json";
const endpointsFiles = [
  "routes/user_game_routes.js",
  "routes/user_biodata.js",
  "routes/game_history.js",
];

swaggerAutogen(outputFile, endpointsFiles).then(() => {
  require("./app.js");
});
