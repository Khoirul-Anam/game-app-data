const express = require("express");
const handleError = require("./error_handler");
const gameHistoryRouter = require("./routes/game_history");
const userBioRouter = require("./routes/user_biodata");
const userGameRouter = require("./routes/user_game_routes");
const swaggerUi = require("swagger-ui-express");
const swaggerFile = require("./swagger_output.json");
const Sentry = require("@sentry/node");
const morgan = require("morgan");
const path = require("path");
require("dotenv").config({ path: "./.env" });

const app = express();
Sentry.init({
  dsn: process.env.SENTRY_DSN,
});
app.use((req, res, next) => {
  req.sentry = Sentry;
  next();
});
app.use(express.static(path.join("public", "res")));
app.use(morgan("tiny"));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use("/doc", swaggerUi.serve, swaggerUi.setup(swaggerFile));
app.get("/", (req, res) => res.redirect("/doc"));
app.use("/", userGameRouter);
app.use("/", userBioRouter);
app.use("/", gameHistoryRouter);
app.use(handleError);

module.exports = app;
