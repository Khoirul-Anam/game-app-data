"use strict";

const loremData =
  "Reprehenderit labore irure magna amet pariatur ullamco nulla. Occaecat Lorem nisi enim veniam officia deserunt enim cupidatat esse aliqua sunt quis. Proident deserunt pariatur duis qui ipsum et ad nulla. Ea est excepteur magna magna reprehenderit magna irure eu officia et ex cillum incididunt id. Qui non excepteur consectetur velit ullamco ea cupidatat sit. Mollit ex commodo voluptate qui duis laborum cillum reprehenderit ullamco veniam incididunt. Cillum pariatur Lorem quis et ipsum.";

module.exports = {
  async up(queryInterface, Sequelize) {
    const arrData = loremData.split(" ");

    const dummyData = Array.from({length: 20}, (_, i)=> ({
      name: arrData[Math.floor(Math.random() * 19) + 1] + arrData[Math.floor(Math.random() * 19) + 1],
      password: arrData[Math.floor(Math.random() * 19) + 1],
      email: 'randommail@gmail.com',
      createdAt: new Date(),
      updatedAt: new Date()
    }));
    console.log(dummyData);

    return queryInterface.bulkInsert('UserGames', dummyData);

    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('UserGames', null, { truncate: true, restartIdentity: true });
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
