"use strict";
const { UserGame, UserBiodata } = require("../models");

const loremData =
  "Aliqua anim reprehenderit anim pariatur dolor magna. Veniam est fugiat tempor in commodo mollit duis consectetur tempor laboris labore et. Eu nostrud non sint proident cupidatat qui occaecat enim in minim excepteur ut aute exercitation. Pariatur esse enim quis do officia ullamco enim commodo. Excepteur laboris ullamco adipisicing nulla aliquip.";
const languageEnum = [
  "English",
  "French",
  "Russian",
  "Japanese",
  "Arab",
  "Chinese",
];
const rankEnum = ["newbie", "rookie", "senior", "pro", "legend"];

module.exports = {
  async up(queryInterface, Sequelize) {
    const nameBox = loremData.split(" ");
    const users = await UserGame.findAll();
    const userIds = users.map(user=>user.id);
    const usersLastId = Math.max(...userIds);

    const dummyData = Array.from({ length: 20 }, (_, i) => ({
      player_name: `${nameBox[Math.floor(Math.random() * 19) + 1]} ${
        nameBox[Math.floor(Math.random() * 19) + 1]
      }`,
      username_id: Math.floor(Math.random() * usersLastId) + 1,
      language:
        languageEnum[Math.floor(Math.random() * languageEnum.length - 1) + 1],
      money: Math.floor(Math.random() * 999999) + 100,
      diamond: Math.floor(Math.random() * 999) + 1,
      rank: rankEnum[Math.floor(Math.random() * rankEnum.length - 1) + 1],
      createdAt: new Date(),
      updatedAt: new Date()
    }));

    await queryInterface.bulkInsert('UserBiodata', dummyData);

  },
  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('UserBiodata', null, { truncate: true, restartIdentity: true });
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
