"use strict";
const { UserBiodata } = require("../models");

const ranks = ["casual", "rank", "ai", "brawl"];
const results = ["lose", "win"];

module.exports = {
  async up(queryInterface, Sequelize) {
    const allUserBio = await UserBiodata.findAll();
    const userBioIds = allUserBio.map((bio) => bio.id);
    const usersBioLastId = Math.max(...userBioIds);
    const dummyData = Array.from({ length: 20 }, (_, i) => {
      const matchResult = results[Math.floor(Math.random() * results.length - 1) + 1];
      const newRank =
        matchResult === "win"
          ? Math.floor(Math.random() * 30) + 1
          : Math.floor(Math.random() * 1) + -29;
      return {
        match_type: ranks[Math.floor(Math.random() * ranks.length - 1) + 1],
        match_date: new Date('2022-04-06'),
        duration_inmin: Math.floor(Math.random() * 40) + 10,
        result: matchResult,
        user_bio_id: Math.floor(Math.random() * usersBioLastId) + 1,
        money_earned: Math.floor(Math.random() * 999) + 100,
        rank_change: newRank,
        createdAt: new Date(),
        updatedAt: new Date(),
      };
    });
    await queryInterface.bulkInsert("UserGameHistories", dummyData);
    
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("UserGameHistories", null, {
      truncate: true,
      restartIdentity: true,
    });
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
