class ErrorResponse {
  static badReq = {
    status: 400,
    message: "Bad request",
  };

  static notFound = {
    status: 404,
    message: "Not found",
  };

  static unAuth = {
    status: 401,
    message: "Unauthorized request",
  };

  static wrongAuth = {
    status: 401,
    message: "Invalid name or password",
  }

  static invalidEmail = {
    status: 400,
    message: "Invalid Email",
  };

  


  static sendCustomResponse = (status, message) => ({
    status,
    message,
  });

}

module.exports = ErrorResponse;
