const express = require("express");
const UserBiodataController = require("../controllers/user_bio_controller");
const userBioRouter = express.Router();
const jwt = require("jsonwebtoken");
const ErrorResponse = require("../error_messages");

userBioRouter.get(
  "/all-bio",
  (req, _res, next) => {
    try {
      const { authorization } = req.headers;
      if (authorization === null || authorization === undefined)
        throw ErrorResponse.unAuth;
      jwt.verify(authorization, "bruh", (err, decodedToken) => {
        if (err) {
          throw {
            status: 401,
            message: "Unauthorized request",
          };
        }
        req.user = decodedToken;
        next();
      });
    } catch (error) {
      next(error);
    }
  },
  UserBiodataController.getAllBio
);

userBioRouter.get(
  "/all-bio/:id",
  (req, _res, next) => {
    try {
      const { authorization } = req.headers;
      if (authorization === null || authorization === undefined)
        throw ErrorResponse.unAuth;
      jwt.verify(authorization, "bruh", (err, decodedToken) => {
        if (err) {
          throw {
            status: 401,
            message: "Unauthorized request",
          };
        }
        req.user = decodedToken;
        next();
      });
    } catch (error) {
      next(error);
    }
  },
  UserBiodataController.getPlayerBio
);

userBioRouter.get(
  "/all-match/:id",
  (req, _res, next) => {
    try {
      const { authorization } = req.headers;
      if (authorization === null || authorization === undefined)
        throw ErrorResponse.unAuth;
      jwt.verify(authorization, "bruh", (err, decodedToken) => {
        if (err) {
          throw {
            status: 401,
            message: "Unauthorized request",
          };
        }
        req.user = decodedToken;
        next();
      });
    } catch (error) {
      next(error);
    }
  },
  UserBiodataController.getPlayerMatches
);

module.exports = userBioRouter;
