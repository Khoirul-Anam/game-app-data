const express = require("express");
const UserController = require("../controllers/user_controller");
const userGameRouter = express.Router();
const { body, validationResult } = require("express-validator");
const ErrorResponse = require("../error_messages");
const jwt = require("jsonwebtoken");
const multer = require("multer");
const storage = require("../config/multer_storage_config");
const authUser = require("../services/auth_service");
const upload = multer({
  storage: storage,
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype === "video/mp4" ||
      file.mimetype === "video/mkv" ||
      file.mimetype === "video/avi" ||
      file.mimetype === "video/webm"
    ) {
      cb(null, true);
    } else {
      cb(new Error("File should be a video"), false);
    }
  },
  limits: {
    fileSize: 30000000,
  },
});

userGameRouter.post("/login", UserController.login);

userGameRouter.post(
  "/register",
  upload.single("video_profile"),
  [
    body("name").notEmpty(),
    body("password").notEmpty(),
    // body("video_profile").notEmpty(),
    body("email").notEmpty()
  ],
  (req, res, next) => {
    const errors = validationResult(req);
    console.log(errors);
    if (!errors.isEmpty()) {
      next(ErrorResponse.badReq);
    } else {
      next();
    }
  },
  UserController.addNewUser
);

userGameRouter.get("/users", authUser, UserController.getAllUser);

userGameRouter.get("/users/:id", authUser, UserController.getUserById);

userGameRouter.put("/users/:id", authUser, UserController.updateUser);

userGameRouter.delete("/users/:id", authUser, UserController.deleteUser);

userGameRouter.post('/forgot-pass-token', UserController.sendForgotPassToken);

userGameRouter.post('/verify-pass-token', UserController.verifyPassToken);

userGameRouter.post('/change-pass', UserController.changePassword);

module.exports = userGameRouter;
