const express = require("express");
const GameHistoryController = require("../controllers/game_history_controller");
const gameHistoryRouter = express.Router();
const jwt = require('jsonwebtoken');
const ErrorResponse = require("../error_messages");

gameHistoryRouter.get('/histories', (req, res, next) => {
    try {
      const { authorization } = req.headers;
      if (authorization === null || authorization === undefined)
        throw ErrorResponse.unAuth;
      jwt.verify(authorization, 'bruh', (err, decodedToken)=>{
        if (err) {
          throw {
            status: 401,
            message: "Unauthorized request",
          };
        }
        req.user = decodedToken;
        next();
      })
    } catch (error) {
      console.log(error);
      next(error);
    }
  }, GameHistoryController.getAllHistories);

gameHistoryRouter.get('/histories/:id', (req, _res, next) => {
    try {
      const { authorization } = req.headers;
      if (authorization === null || authorization === undefined)
        throw ErrorResponse.unAuth;
      jwt.verify(authorization, 'bruh', (err, decodedToken)=>{
        if (err) {
          throw {
            status: 401,
            message: "Unauthorized request",
          };
        }
        req.user = decodedToken;
        next();
      })
    } catch (error) {
      console.log(error);
      next(error);
    }
  }, GameHistoryController.getHistoryById);
  

module.exports = gameHistoryRouter;