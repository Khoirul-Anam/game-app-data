# Program RESTful API Game Data
*by: Mohammad Khoirul Anam*

## Menjalankan server
```
npm run start-dev
atau
npm run start

```

## Daftar Route Tersedia

### User Game:

```
http://localhost:5000/users (GET)
http://localhost:5000/users (POST)
http://localhost:5000/users/:id (GET)
http://localhost:5000/users/:id (PUT)
http://localhost:5000/users/:id (DELETE)
```

### User Biodata
```
http://localhost:5000/all-bio (GET)
http://localhost:5000/all-bio/:id (GET)
http://localhost:5000/all-match/:id (GET)
```

### User Game History
```
http://localhost:5000/histories (GET)
http://localhost:5000/histories/:id (GET)
```