
const { UserGame } = require("../models");
const { OAuth2Client } = require("google-auth-library");
const client = new OAuth2Client(
  process.env.GOOGLE_CLIENT_ID,
  process.env.GOOGLE_CLIENT_SECRET
);
const axios = require("axios");
const jwt = require("jsonwebtoken");
const ErrorResponse = require("../error_messages");
const MessageTemplate = require("./msg_template_service");
const sendEmail = require('../services/email_service')
const bcrypt = require('bcryptjs');

class LoginMethod {
    static googleLogin = async (google_id_token) => {
      console.log(process.env.GOOGLE_CLIENT_ID);
      const payload = await client.verifyIdToken({
        idToken: google_id_token,
        requiredAudience: process.env.GOOGLE_CLIENT_ID,
      });
      const user = await UserGame.findOne({
        where: {
          name: payload.payload.name,
        },
      });
      if (!user) {
        const createdUser = await UserGame.create({
          name: payload.payload.name,
          password: '',
          email: payload.payload.email,
        });
        const token = jwt.sign(
          {
            id: createdUser.id,
            name: createdUser.name,
          },
          process.env.JWT_SECRET
        );
        const messageContent = MessageTemplate.getRegMsg(payload.payload.name);
        await sendEmail(
          process.env.SERVER_EMAIL,
          payload.payload.email,
          null,
          messageContent.message,
          messageContent.subject
        );
        return token
      }
      const token = jwt.sign(
        {
          id: user.id,
          name: user.name,
        },
        process.env.JWT_SECRET
      );
      return token
    };
  
    static facebookLogin = async (facebook_id_token) => {
      const response = await axios.get(
        `https://graph.facebook.com/v14.0/me?fields=id%2Cname%2Cemail%2Cgender%2Cbirthday&access_token=${facebook_id_token}`
      );
      console.log(response);
      const user = await UserGame.findOne({
        where: {
          name: response.data.name,
        },
      });
      if (!user) {
        const createdUser = await UserGame.create({
          name: response.data.name,
          password: '',
          email: response.data.email,
        });
  
        const token = jwt.sign(
          {
            id: createdUser.id,
            name: createdUser.name,
          },
          process.env.JWT_SECRET
        );
        const messageContent = MessageTemplate.getRegMsg(createdUser.name);
        await sendEmail(
          process.env.SERVER_EMAIL,
          createdUser.email,
          null,
          messageContent.message,
          messageContent.subject
        );
        return token
      }
      const token = jwt.sign(
        {
          id: user.id,
          name: user.name,
        },
        process.env.JWT_SECRET
      );
      return token;
    };
  
    static manualLogin = async (name, password) => {
      const user = await UserGame.findOne({
        where: {
          name,
        },
      });
      if (!bcrypt.compareSync(password, user.password))
        throw ErrorResponse.wrongAuth;
      const token = jwt.sign(
        {
          id: user.id,
          name: user.name,
        },
        process.env.JWT_SECRET
      );
      console.log(`secret key: ${process.env.JWT_SECRET}`);
      return token
    };
  }
  

  module.exports = LoginMethod