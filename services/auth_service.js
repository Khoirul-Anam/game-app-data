const jwt = require('jsonwebtoken');
const ErrorResponse = require('../error_messages');

module.exports = (req, _res, next) => {
    try {
      const { authorization } = req.headers;
      if (authorization === null || authorization === undefined)
        throw ErrorResponse.unAuth;
      jwt.verify(authorization, "bruh", (err, decodedToken) => {
        if (err) {
          throw ErrorResponse.unAuth;
        }
        console.log(decodedToken);
        req.user = decodedToken;
        next();
      });
    } catch (error) {
      next(error);
    }
  }