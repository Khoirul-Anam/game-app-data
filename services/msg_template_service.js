class MessageTemplate {
    static getRegMsg(user) {
      return {
        subject: "Berhasil Registrasi",
        message: `Registrasi berhasil... \n Selamat datang ${user}..`,
      };
    }
  
    static getFrgPassMsg(token, date) {
      return {
        subject: "Kode Verifikasi Lupa Password",
        message: `Kode verifikasi token adalah ${token} \n\n\n\n Email dibuat pada ${date}`,
      };
    }
  
  }
  
  module.exports = MessageTemplate
  