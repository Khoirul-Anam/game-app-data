const request = require("supertest");
const { sequelize } = require("../models/index");
const { queryInterface } = sequelize;
const bcrypt = require("bcryptjs");
const app = require("../app");
const jwt = require("jsonwebtoken");

let token;

beforeEach(async () => {
  const salt = bcrypt.genSaltSync(10);
  const hash = bcrypt.hashSync("qweqwe", salt);

  const dummyData = Array.from({ length: 4 }, (_, i) => ({
    name: "Budi",
    password: hash,
    createdAt: new Date(),
    updatedAt: new Date(),
  }));
  token = jwt.sign({ id: 2, name: "Susi" }, 'bruh');

  await queryInterface.bulkInsert("UserGames", dummyData);
});

afterEach(async () => {
  await queryInterface.bulkDelete(
    "UserGames",
    {},
    { truncate: true, restartIdentity: true }
  );
});

describe("Login User /login", () => {
  it("success", (done) => {
    request(app)
      .post("/login")
      .send({
        name: "Budi",
        password: "qweqwe",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(res.body).toHaveProperty("token");
          done();
        }
      });
  });

  it("Wrong name", (done) => {
    request(app)
      .post("/login")
      .send({
        name: "unknown user",
        password: "qweqwe",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Invalid name or password");
          done();
        }
      });
  });
  it("Wrong password", (done) => {
    request(app)
      .post("/login")
      .send({
        name: "Budi",
        password: "wrong password",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Invalid name or password");
          done();
        }
      });
  });
});

describe("Tambah User baru /users", () => {
  it("success", (done) => {
    request(app)
      .post("/users")
      .set("authorization", token)
      .send({
        name: "Mahmud",
        password: "qweqwe",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toEqual(201);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Berhasil menambahkan user");
          done();
        }
      });
  });

  it("no auth", (done) => {
    request(app)
      .post("/users")
      .send({
        name: "new user",
        password: "qweqwe",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("invalid token", (done) => {
    request(app)
      .post("/users")
      .set("authorization", "hehe")
      .send({
        name: "new user",
        password: "qweqwe",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("required field violation", (done) => {
    request(app)
      .post("/users")
      .set("authorization", token)
      .send({
        name: "new user",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(400);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Bad request");
          done();
        }
      });
  });
  it("password length less than 5", (done) => {
    request(app)
      .post("/users")
      .set("authorization", token)
      .send({
        name: "Susi",
        password: "halo",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(400);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Bad request");
          done();
        }
      });
  });
});

describe("Lihat daftar User /users", () => {
  it("success", (done) => {
    request(app)
      .get("/users")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(Array.isArray(res.body)).toBe(true);
          expect(res.body[0]).toHaveProperty("id");
          expect(res.body[0]).toHaveProperty("name");
          done();
        }
      });
  });

  it("success", (done) => {
    request(app)
      .get("/users?name=Budi")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(Array.isArray(res.body)).toBe(true);
          expect(res.body[0]).toHaveProperty("id");
          expect(res.body[0]).toHaveProperty("name");
          done();
        }
      });
  });

  it("no auth", (done) => {
    request(app)
      .get("/users")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("invalid token", (done) => {
    request(app)
      .get("/users")
      .set("authorization", "hehe")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("Users not found with query params", (done) => {
    request(app)
      .get("/users?name=nama-asal")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(404);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Not found");
          done();
        }
      });
  });
});

describe("Lihat User Berdasar Id /users/:id", () => {
  it("success", (done) => {
    request(app)
      .get("/users/2")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(res.body).toBeInstanceOf(Object);
          expect(res.body).toHaveProperty("id");
          expect(res.body).toHaveProperty("name");
          expect(res.body.name).toBe("Budi");
          done();
        }
      });
  });
  it("no auth", (done) => {
    request(app)
      .get("/users/2")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("invalid token", (done) => {
    request(app)
      .get("/users/2")
      .set("authorization", "hehe")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("User not found", (done) => {
    request(app)
      .get("/users/99")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(404);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Not found");
          done();
        }
      });
  });
});


describe("Update  User /users/:id", () => {
  it("success", (done) => {
    request(app)
      .put("/users/2")
      .set("authorization", token)
      .send({
        name: "Budi baru",
        password: "new Password",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Berhasil update data");
          done();
        }
      });
  });

  it("success with only one field", (done) => {
    request(app)
      .put("/users/2")
      .set("authorization", token)
      .send({
        password: "new Password",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Berhasil update data");
          done();
        }
      });
  });

  it("required field violation", (done) => {
    request(app)
      .put("/users/2")
      .set("authorization", token)
      .send({})
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(400);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Bad request");
          done();
        }
      });
  });

  it("no auth", (done) => {
    request(app)
      .put("/users/2")
      .send({
        name: "new name",
        password: "new password",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("invalid token", (done) => {
    request(app)
      .put("/users/2")
      .set("authorization", "hehe")
      .send({
        name: "new name",
        password: "new password",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("user who is going to be updated not found", (done) => {
    request(app)
      .put("/users/99")
      .set("authorization", token)
      .send({
        name: "new name",
        password: "new password",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(404);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Not found");
          done();
        }
      });
  });
});

describe("Delete User /users/:id", () => {
  it("success", (done) => {
    request(app)
      .delete("/users/2")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Berhasil menghapus data");
          done();
        }
      });
  });


  it("no auth", (done) => {
    request(app)
      .delete("/users/2")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("invalid token", (done) => {
    request(app)
      .delete("/users/2")
      .set("authorization", "hehe")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("user not found", (done) => {
    request(app)
      .put("/users/99")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(404);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Not found");
          done();
        }
      });
  });
});
