const request = require("supertest");
const { sequelize } = require("../models/index");
const { queryInterface } = sequelize;
const bcrypt = require("bcryptjs");
const app = require("../app");
const jwt = require("jsonwebtoken");

let token;

beforeEach(async () => {
  const salt = bcrypt.genSaltSync(10);
  const hash = bcrypt.hashSync("qweqwe", salt);

  const dummyData = Array.from({ length: 2 }, (_, i) => ({
    name: "Budi",
    password: hash,
    createdAt: new Date(),
    updatedAt: new Date(),
  }));
  token = jwt.sign({ id: 2, name: "Budi" }, "bruh");
  const bioDummyData = [
    {
        player_name: "Budi_tampan",
        username_id: 1,
        language: "French",
        money: 80000,
        diamond: 90,
        rank: "senior",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    {
        player_name: "Budi_tampan",
        username_id: 2,
        language: "French",
        money: 80000,
        diamond: 90,
        rank: "senior",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        player_name: "Budi_tampan",
        username_id: 3,
        language: "French",
        money: 80000,
        diamond: 90,
        rank: "senior",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
  ];

  const historyDummyData = Array.from({ length: 4 }, (_, i) => ({
    match_type: "rank",
    match_date: new Date("2022-05-07"),
    duration_inmin: 40,
    result: "lose",
    user_bio_id: 2,
    money_earned: 6000,
    createdAt: new Date(),
    updatedAt: new Date(),
  }));

  await queryInterface.bulkInsert("UserGames", dummyData);
  await queryInterface.bulkInsert("UserBiodata", bioDummyData);
  await queryInterface.bulkInsert("UserGameHistories", historyDummyData);
});

afterEach(async () => {
  await queryInterface.bulkDelete(
    "UserGames",
    {},
    { truncate: true, restartIdentity: true }
  );
  await queryInterface.bulkDelete(
    "UserBiodata",
    {},
    { truncate: true, restartIdentity: true }
  );
  await queryInterface.bulkDelete(
    "UserGameHistories",
    {},
    { truncate: true, restartIdentity: true }
  );
});

describe("Lihat daftar Bio /all-bio", () => {
  it("success", (done) => {
    request(app)
      .get("/all-bio")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(res.body).toBeInstanceOf(Array);
          expect(res.body[0]).toHaveProperty("id");
          expect(res.body[0]).toHaveProperty("player_name");
          done();
        }
      });
  });

  it("no auth", (done) => {
    request(app)
      .get("/all-bio")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("invalid token", (done) => {
    request(app)
      .get("/all-bio")
      .set("authorization", "hehe")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });
});

describe("Lihat Biodata Berdasar Id /all-bio/:id", () => {
  it("success", (done) => {
    request(app)
      .get("/all-bio/2")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(res.body).toBeInstanceOf(Object);
          expect(res.body).toHaveProperty("id");
          expect(res.body).toHaveProperty("player_name");
          expect(res.body).toHaveProperty("username_id");
          expect(res.body).toHaveProperty("language");
          expect(res.body).toHaveProperty("money");
          expect(res.body).toHaveProperty("diamond");
          expect(res.body).toHaveProperty("rank");
          expect(res.body).toHaveProperty("createdAt");
          expect(res.body).toHaveProperty("updatedAt");
          done();
        }
      });
  });
  it("no auth", (done) => {
    request(app)
      .get("/all-bio/2")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("bio milik orang", (done) => {
    request(app)
      .get("/all-bio/3")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("invalid token", (done) => {
    request(app)
      .get("/all-bio/2")
      .set("authorization", "hehe")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("Bio not found", (done) => {
    request(app)
      .get("/all-bio/99")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(404);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Not found");
          done();
        }
      });
  });
});

describe("Daftar pertandingan dari suatu bio /all-match/:id", () => {
  it("success", (done) => {
    request(app)
      .get("/all-match/2")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(res.body).toBeInstanceOf(Object);
          expect(res.body).toHaveProperty("user");
          expect(res.body.user).toBe("Budi");
          expect(res.body.match_detail).toBeInstanceOf(Object);
          expect(res.body.match_detail.histories).toBeInstanceOf(Array);
          done();
        }
      });
  });

  it("no auth", (done) => {
    request(app)
      .get("/all-match/2")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("invalid token", (done) => {
    request(app)
      .get("/all-match/2")
      .set("authorization", "hehe")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });
  it("Bio not found", (done) => {
    request(app)
      .get("/all-match/99")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(404);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Not found");
          done();
        }
      });
  });
});
