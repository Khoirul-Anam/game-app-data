const request = require("supertest");
const { sequelize } = require("../models/index");
const { queryInterface } = sequelize;
const bcrypt = require("bcryptjs");
const app = require("../app");
const jwt = require("jsonwebtoken");

let token;

beforeEach(async () => {
  const salt = bcrypt.genSaltSync(10);
  const hash = bcrypt.hashSync("qweqwe", salt);

  token = jwt.sign({ id: 2, name: "Budi" }, "bruh");
  const bioDummyData = [
    {
        player_name: "Budi_tampan",
        username_id: 1,
        language: "French",
        money: 80000,
        diamond: 90,
        rank: "senior",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    {
        player_name: "Budi_tampan",
        username_id: 2,
        language: "French",
        money: 80000,
        diamond: 90,
        rank: "senior",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        player_name: "Budi_tampan",
        username_id: 3,
        language: "French",
        money: 80000,
        diamond: 90,
        rank: "senior",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
  ];

  const historyDummyData = Array.from({ length: 4 }, (_, i) => ({
    match_type: "rank",
    match_date: new Date("2022-05-07"),
    duration_inmin: 40,
    result: "lose",
    user_bio_id: 2,
    money_earned: 6000,
    createdAt: new Date(),
    updatedAt: new Date(),
  }));


  await queryInterface.bulkInsert("UserBiodata", bioDummyData);
  await queryInterface.bulkInsert("UserGameHistories", historyDummyData);
});

afterEach(async () => {
  
  await queryInterface.bulkDelete(
    "UserBiodata",
    {},
    { truncate: true, restartIdentity: true }
  );
  await queryInterface.bulkDelete(
    "UserGameHistories",
    {},
    { truncate: true, restartIdentity: true }
  );
});

describe("Lihat daftar History /histories", () => {
    it("success", (done) => {
      request(app)
        .get("/histories")
        .set("authorization", token)
        .end((err, res) => {
          if (err) {
            done(err);
          } else {
            expect(res.status).toBe(200);
            expect(res.body).toBeInstanceOf(Array);
            expect(res.body[0]).toHaveProperty("id");
            expect(res.body[0]).toHaveProperty("match_type");
            expect(res.body[0]).toHaveProperty("match_date");
            done();
          }
        });
    }, 9000);
  
    it("no auth", (done) => {
      request(app)
        .get("/histories")
        .end((err, res) => {
          if (err) {
            done(err);
          } else {
            expect(res.status).toBe(401);
            expect(res.body).toHaveProperty("message");
            expect(res.body.message).toBe("Unauthorized request");
            done();
          }
        });
    });
  
    it("invalid token", (done) => {
      request(app)
        .get("/histories")
        .set("authorization", "hehe")
        .end((err, res) => {
          if (err) {
            done(err);
          } else {
            expect(res.status).toBe(401);
            expect(res.body).toHaveProperty("message");
            expect(res.body.message).toBe("Unauthorized request");
            done();
          }
        });
    });
  });
  
  describe("Lihat History Berdasar Id /histories/:id", () => {
    it("success", (done) => {
      request(app)
        .get("/histories/2")
        .set("authorization", token)
        .end((err, res) => {
          if (err) {
            done(err);
          } else {
            expect(res.status).toBe(200);
            expect(res.body).toBeInstanceOf(Object);
            expect(res.body).toHaveProperty("id");
            expect(res.body).toHaveProperty("match_type");
            expect(res.body).toHaveProperty("match_date");
            expect(res.body).toHaveProperty("duration_inmin");
            expect(res.body).toHaveProperty("result");
            expect(res.body).toHaveProperty("user_bio_id");
            expect(res.body).toHaveProperty("money_earned");
            expect(res.body).toHaveProperty("createdAt");
            expect(res.body).toHaveProperty("updatedAt");
            done();
          }
        });
    }, 9000);
    it("no auth", (done) => {
      request(app)
        .get("/histories/2")
        .end((err, res) => {
          if (err) {
            done(err);
          } else {
            expect(res.status).toBe(401);
            expect(res.body).toHaveProperty("message");
            expect(res.body.message).toBe("Unauthorized request");
            done();
          }
        });
    });

    it("invalid token", (done) => {
        request(app)
          .get("/histories/2")
          .set("authorization", "hehe")
          .end((err, res) => {
            if (err) {
              done(err);
            } else {
              expect(res.status).toBe(401);
              expect(res.body).toHaveProperty("message");
              expect(res.body.message).toBe("Unauthorized request");
              done();
            }
          });
      });
  
    it("history tidak ditemukan", (done) => {
      request(app)
        .get("/histories/90")
        .set("authorization", token)
        .end((err, res) => {
          if (err) {
            done(err);
          } else {
            expect(res.status).toBe(404);
            expect(res.body).toHaveProperty("message");
            expect(res.body.message).toBe("Not found");
            done();
          }
        });
    });
  });