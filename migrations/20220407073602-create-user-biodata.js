"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("UserBiodata", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      player_name: {
        type: Sequelize.STRING,
      },
      username_id: {
        type: Sequelize.INTEGER,
      },
      language: {
        type: Sequelize.ENUM(
          "English",
          "French",
          "Russian",
          "Japanese",
          "Arab",
          "Chinese"
        ),
      },
      money: {
        type: Sequelize.BIGINT,
      },
      diamond: {
        type: Sequelize.INTEGER,
      },
      rank: {
        type: Sequelize.ENUM("newbie", "rookie", "senior", "pro", "legend"),
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.dropTable("UserBiodata"),
        queryInterface.sequelize.query(
          'DROP TYPE IF EXISTS "enum_UserBiodata_language";'
        ),
        queryInterface.sequelize.query(
          'DROP TYPE IF EXISTS "enum_UserBiodata_rank";'
        ),
      ]);
    });
  },
};
