'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn('UserGames', 'forgot_pass_token', { type: Sequelize.STRING });
    await queryInterface.addColumn('UserGames', 'forgot_pass_token_expired_at', { type: Sequelize.DATE });

    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.removeColumn('UserGames', 'forgot_pass_token', { /* query options */ });
    await queryInterface.removeColumn('UserGames', 'forgot_pass_token_expired_at', { /* query options */ });

    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
