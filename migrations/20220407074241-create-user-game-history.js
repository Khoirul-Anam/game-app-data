'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('UserGameHistories', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      match_type: {
        type: Sequelize.ENUM('casual', 'rank', 'ai', 'brawl')
      },
      match_date: {
        type: Sequelize.DATE
      },
      duration_inmin: {
        type: Sequelize.INTEGER
      },
      result: {
        type: Sequelize.ENUM('lose', 'win')
      },
      user_bio_id: {
        type: Sequelize.INTEGER
      },
      money_earned: {
        type: Sequelize.BIGINT
      },
      rank_change: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('UserGameHistories');
  }
};