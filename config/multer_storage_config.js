const multer = require('multer');
const fs = require('fs');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      if (fs.existsSync('public/res/')) {
        cb(null, 'public/res')
      } else {
        fs.mkdirSync('public/res', {recursive: true})
        cb(null, 'public/res')
      }
    },
    filename: function (req, file, cb) {
      const uniqueSuffix = Date.now() + '-' + file.originalname
      cb(null, file.fieldname + '-' + uniqueSuffix)
    }
  })
  
  module.exports = storage
